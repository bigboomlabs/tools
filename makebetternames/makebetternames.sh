#!/bin/sh
########################################################################
# MakeBetterNames v.1.0
# by A Boy Named Boom (http://bigboomlabs.com)
# 2014/04/23
########################################################################
#DEFINE FUNCTIONS
FIND_FUNCTION="/usr/bin/find";
MD5_FUNCTION="/usr/bin/md5sum";
CP_FUNCTION="/usr/local/bin/cvcp";
BASENAME_FUNCTION="/bin/basename";
########################################################################
CAMERA=$1;
SHOOTDATE=$2;
PATH=$3;

if [[ -z "$1" ]]; then
	echo "CAMERA is null.";
	echo "Usage : /usr/local/bin/makebetternames.sh CAMERA SHOOTDATE PATH";
	echo "Usage : /usr/local/bin/makebetternames.sh E002 20140422 /Volumes/Media/Drop01/ECAM/E022";
	exit 1
fi
if [[ -z "$2" ]]; then
	echo "SHOOTDATE is null.";
	echo "Usage : /usr/local/bin/makebetternames.sh CAMERA SHOOTDATE PATH";
	echo "Usage : /usr/local/bin/makebetternames.sh E002 20140422 /Volumes/Media/Drop01/ECAM/E022";
	exit 1
fi
if [[ -z "$3" ]]; then
	echo "PATH is null.";
	echo "Usage : /usr/local/bin/makebetternames.sh CAMERA SHOOTDATE PATH";
	echo "Usage : /usr/local/bin/makebetternames.sh E002 20140422 /Volumes/Media/Drop01/ECAM/E022";
	exit 1
fi

if [ -d $PATH ]; then
    VERIFIED_PATH=1;
else
    echo "THERE SEEMS TO BE A PROBLEM WITH YOUR FILEPATH"
    exit 1
fi
########################################################################
#GET OUR LIST OF FILES
FILES=$($FIND_FUNCTION $PATH -depth -type f ! -name ".*")

exec $(echo "$PATH" >> $PATH/MAKEBETTERNAMESCONVERSIONLOG.txt)

#START PROCESS
for i in $FILES
do 
	#MAKE MD5 FOR SOURCE FILENAME
	RESPONSE=$($MD5_FUNCTION $i)
	echo $RESPONSE;
	#WRITE MD5 TO LOG FILE
	exec $(echo "$RESPONSE" >> $PATH/MAKEBETTERNAMESCONVERSIONLOG.txt)

	#PREP FILE FOR COPY
	FILENAME=$($BASENAME_FUNCTION "$i")
	EXTENSION="${FILENAME##*.}"
	FILEROOT="${FILENAME%.*}"

	NEWFILENAME=$PATH/"$CAMERA"_"$SHOOTDATE"_$FILEROOT.$EXTENSION;
	
	#COPY FILE
	$CP_FUNCTION $i $NEWFILENAME;
	
	#MAKE NEW FILE MD5
	RESPONSE=$($MD5_FUNCTION $NEWFILENAME)
	echo $RESPONSE;
	
	#WRITE NEW FILE MD5	
	exec $(echo "$RESPONSE" >> $PATH/MAKEBETTERNAMESCONVERSIONLOG.txt)
	
	
#CLOSE FILE LOOP
done

exit 0
