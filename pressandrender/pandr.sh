#Press & Render v.1.0
#by A Boy Named Boom (http://bigboomlabs.com)
#2014/04/23

SITE="boomexplosion.com";

#!/bin/sh
wget --content-disposition --mirror -r -p -e robots=off http://dev.$SITE

find "dev".$SITE -name '*.html*' -exec sed -i "s/http:\/\/dev.$SITE/http:\/\/$SITE/g" {} \;

FILES=$(find "dev".$SITE -depth -type f -name "*\?*")
for i in $FILES
do 

	SUBSTRING=`echo $i | cut -f1 -d"?"`
	mv $i $SUBSTRING;

done

cp -rf dev.$SITE/* /var/www/vhosts/$SITE/htdocs/
exit 0